controllers.controller('LightsCtrl', function($scope, LightService, $timeout, ionicMaterialMotion, ionicMaterialInk) {
	// definitions and initializations
	var lightsboard = this;
	var animateflag = false;
	lightsboard.lights = [];
	lightsboard.rooms = [];

	// events
	$scope.$on('rooms', function (event) {
		refresh(animateflag);
	});
	$scope.$on('lights', function (event) {
		refresh(animateflag);
	});
	$scope.$on('light', function (event, id) {
		refresh(animateflag, id);
	});
	$scope.$on('$ionicView.enter',function (e) {
		refresh(animateflag);
	});
	$scope.$on('lights-toggle', function (event) {
		lightsboard.lights = LightService.getLights();
		$scope.$apply(function  () {
			_.map(lightsboard.lights, function (light) {
				return LightService.getLight(light.id)
			});
		})
				refresh(animateflag);
	});
	$scope.$on('ngLastRepeat.lights', function (event) {
		animateflag = false;
		animate();
	});
	$scope.allOn = function () {
		LightService.allOn();
	};
	$scope.allOff = function () {
		LightService.allOff();
	};
	// bound functions
	$scope.update_state = function (light) {
		if (light.state == true) LightService.switchOn(light.id);
		if (light.state == false) LightService.switchOff(light.id);
	};
	$scope.roomOn = function(currentRoom){
		_.forEach(lightsboard.lights, function(value){
			if(value.room == currentRoom.id){
				LightService.switchOn(value.id);
			}
		});		
	}
	$scope.roomOff = function(room){
		_.forEach(lightsboard.lights, function(value){
			if(value.room == room.id){
				LightService.switchOff(value.id);
			}
		});

	}
	$scope.refreshpage = function () {
		refresh(false);
	};
	// internal functions
	var populateRooms = function (roomsArray){
		if(roomsArray === 'undefined') return null;
		
		else {
			_.forEach(roomsArray, function(value, key){
				var thisRoom = roomsArray[key].id;
				var theseLights = [];
				_.forEach(lightsboard.lights, function(value, key){
					if(lightsboard.lights[key].room === thisRoom){
						theseLights= _.concat(theseLights,lightsboard.lights[key]);
					}
				});
				roomsArray[key].lights = theseLights;
			});
			return roomsArray;
		}	
	};

	var refresh = function (flag, id) {
		$scope.$apply(function () {
			if (typeof id === 'undefined') {
				console.log(flag, id);
				lightsboard.lights = LightService.getLights();
				lightsboard.rooms = populateRooms(LightService.getRooms());
				console.log(lightsboard.lights);
				console.log(lightsboard.rooms);
				if (flag===false) animate();
				else { $scope.$broadcast('scroll.refreshComplete'); }
			}
			if (typeof id !== 'undefined') {
				_.map(lightsboard.lights, function (light) {
					if (light.id == id) return LightService.getLight(id);
					else return light;
				});
				$scope.$broadcast('scroll.refreshComplete');
			}
		});
	}	
	var animate = function () {
		$scope.$parent.showHeader();
		$scope.$parent.clearFabs();
		$scope.isExpanded = true;
		$scope.$parent.setExpanded(true);
		$scope.$parent.setHeaderFab('right');
		// Activate ink for controller
		ionicMaterialInk.displayEffect();
		$timeout(function () {
			ionicMaterialMotion.fadeSlideIn({
				selector: ' .item'
			});
			$scope.$broadcast('scroll.refreshComplete');
		}, 200);
	};
});

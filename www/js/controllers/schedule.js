controllers.controller('ScheduleCtrl', function($scope, $stateParams, ScheduleService, FormatService, $timeout, ionicMaterialMotion, ionicMaterialInk, ionicTimePicker, BroadcastService, LightService) {
	var scheduleboard = this;
	var animateflag = false;
	scheduleboard.edited = false;
	scheduleboard.initData = {};
	scheduleboard.task = {};
	scheduleboard.lights = [];
	scheduleboard.isDaysValid = true;
	scheduleboard.lastDaysIndex = 7;
	$scope.$on('$ionicView.enter',function (e) {
		console.log('ionicView.enter called')
		animateflag = true;
		refresh(animateflag);
	});
	$scope.$on('tasks', function (event) {
		refresh(animateflag);
	});
	$scope.$on('task', function (event, id) {
		refresh(animateflag, id);
	});
	$scope.$on('task.cron', function (event, id) {
		scheduleboard.edited = false;
		refresh(animateflag, id);
	});
	$scope.refreshpage = function () {
		refresh(false);
	};
	$scope.ontimePicker = function () {
		ionicTimePicker.openTimePicker({
			callback: function (val) {
				if (val != scheduleboard.task.ontime.secs) {
					scheduleboard.task.ontime.secs = val;
					if (hasDataDiffed()) scheduleboard.edited = true;
					else scheduleboard.edited = false;
					scheduleboard.task.ontime.cron = FormatService.updateCronTime(scheduleboard.task.ontime.cron, val);
					scheduleboard.task.ontime.text = FormatService.getTimeText(scheduleboard.task.ontime.cron);
					console.log(scheduleboard.edited);
					console.log(hasDataDiffed());
				}
			},
			inputTime: scheduleboard.task.ontime.secs,
			format: 12,
			step: 5,
			setLabel: 'Set'
		});
	};
	$scope.offtimePicker = function () {
		ionicTimePicker.openTimePicker({
			callback: function (val) {
				if (val != scheduleboard.task.offtime.secs) {
					scheduleboard.task.offtime.secs = val;
					if (hasDataDiffed()) scheduleboard.edited = true;
					else scheduleboard.edited = false;
					scheduleboard.task.offtime.cron = FormatService.updateCronTime(scheduleboard.task.offtime.cron, val);
					scheduleboard.task.offtime.text = FormatService.getTimeText(scheduleboard.task.offtime.cron);
					console.log(scheduleboard.edited);
					console.log(hasDataDiffed());
				}
			},
			inputTime: scheduleboard.task.offtime.secs,
			format: 12,
			step: 5,
			setLabel: 'Set'
		});
	};
	$scope.setstate = function (task) {
		if (task.state == true) ScheduleService.switchOn(task.id);
		if (task.state == false) ScheduleService.switchOff(task.id);
	};
	$scope.onDayChanged = function () {
		if (hasDataDiffed()) scheduleboard.edited = true;
		else scheduleboard.edited = false;
		console.log(scheduleboard.task.days.bool);
		scheduleboard.task.ontime.cron = FormatService.updateCronDays(scheduleboard.task.ontime.cron, scheduleboard.task.days.bool);
		setOffTimeCorrectly();
	};
	$scope.setTime = function () {
		scheduleboard.task.ontime.cron = FormatService.updateCronDays(scheduleboard.task.ontime.cron, scheduleboard.task.days.bool);
		setOffTimeCorrectly();
		ScheduleService.setCron(scheduleboard.task.id, scheduleboard.task.ontime.cron, scheduleboard.task.offtime.cron);
	};
	$scope.getLightName = function(id){
		var thisLight = LightService.getLight(id);
		console.log(thisLight);
		return thisLight.name;
	}
	var hasDataDiffed = function () {
		console.log(scheduleboard.initData.days);
		console.log(scheduleboard.task.days.bool);
		return (scheduleboard.initData != {
			on: scheduleboard.task.ontime.secs,
			off: scheduleboard.task.offtime.secs,
			days: scheduleboard.task.days.bool
		});
	}
	var setOffTimeCorrectly = function () {
		console.log(scheduleboard.task.ontime.secs)
		console.log(scheduleboard.task.offtime.secs)
		if (scheduleboard.task.ontime.secs > scheduleboard.task.offtime.secs) {
			scheduleboard.task.offtime.cron = FormatService.makeCronNextDays(scheduleboard.task.days.bool, scheduleboard.task.offtime.cron);
		} else {
			scheduleboard.task.offtime.cron = FormatService.updateCronDays(scheduleboard.task.offtime.cron, scheduleboard.task.days.bool);
		}
		console.log(scheduleboard.task.offtime.cron);
	};
	var refresh = function (flag, id) {
		$scope.$apply(function () {
			var task = ScheduleService.getTask($stateParams.id);
			scheduleboard.initData = {
				on: task.ontime.secs,
				off: task.offtime.secs,
				days: task.days.bool
			};
			scheduleboard.task = task;
			console.log(scheduleboard.task);
			console.log(scheduleboard.initData);
			if (flag===true) animate();
			else { $scope.$broadcast('scroll.refreshComplete'); }
		});
		BroadcastService('taskupdate');

	};
	var animate = function () {
		$scope.$parent.showHeader();
		$scope.$parent.clearFabs();
		$scope.isExpanded = true;
		$scope.$parent.setExpanded(true);
		$scope.$parent.setHeaderFab('right');
		// Activate ink for controller
		ionicMaterialInk.displayEffect();
		$timeout(function () {
			ionicMaterialMotion.fadeSlideIn({
				selector: '.animate-blinds .item'
			});
			$scope.$broadcast('scroll.refreshComplete');
		}, 200);
	};
});

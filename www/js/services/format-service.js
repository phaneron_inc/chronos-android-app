services.factory('FormatService', function($rootScope) {
	var formatService = this;
	var informat = 'YYYY/MM/DD HH:mm:ss ZZ';
	var outformat = 'llll';
	var daysStringShort = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
	var daysStringLong = ['Monday','Tueday','Wednesday','Thursday','Friday','Saturday','Sunday'];
	var cronToTimeText = function (cronstring) {
		console.log('cronstring', cronstring);
		var cronarray = cronstring.split(' ');
		return pad(cronarray[2],2)+':'+pad(cronarray[1],2);
		// if (cronarray[2] < 12) return pad(cronarray[2],2)+':'+pad(cronarray[1],2)+' AM';
		// if (cronarray[2] = 12) return pad(cronarray[2],2)+':'+pad(cronarray[1],2)+' PM';
		// if (cronarray[2] > 12) return pad(cronarray[2]-12,2)+':'+pad(cronarray[1],2)+' PM';
	};
	var cronToTimeSecs = function (cronstring) {
		var cronarray = cronstring.split(' ');
		// console.log(cronarray);
		return cronarray[2]*3600 + cronarray[1]*60;
	};
	var cronToDaysBool = function (cronstring) {
		var cronarray = cronstring.split(' ');
		if (cronarray[5] == '*') {
			// console.log('loop 1');
			return [true, true, true, true, true, true, true];
		} else if (cronarray[5].split('-').length == 1 && cronarray[5].split(',').length == 1 && parseInt(cronarray[5]) >= 0 && parseInt(cronarray[5]) < 7) {
			// console.log('loop 2');
			var arr = [false, false, false, false, false, false, false];
			arr[parseInt(cronarray[5])] = true;
			return arr;
		} else if (cronarray[5].split(',').length > 1) {
			// console.log('loop 3');
			var daybools = [false, false, false, false, false, false, false];
			for (var i = 0; i <= 6; i++) {
				if (cronarray[5].match(i) != null) daybools[i] = true;
			}
			return daybools;
		} else if (cronarray[5].split(',').length == 1) {
			// console.log('loop 4');
			start = cronarray[5].split('-')[0];
			end = cronarray[5].split('-')[1];
			var daybools = [false, false, false, false, false, false, false];
			for (var i = start; i <= end; i++) {
				daybools[i] = true;
			}
			return cronarray;
		}
		else return null
	};
	var cronToDaysText = function (cronstring, type) {
		var days = getDaysNames(type);
		var cronarray = cronstring.split(' ');
		if (cronarray[5] == '*') {
			// console.log(', loop 1');
			return 'Everyday';
		} else if (cronarray[5].split('-').length == 1 && cronarray[5].split(',').length == 1 && parseInt(cronarray[5]) >= 0 && parseInt(cronarray[5]) < 7) {
			// console.log(', loop 2');
			return days[parseInt(cronarray[5])];
		} else if (cronarray[5] == '0,6' || cronarray[5] == '6,0') {
			return 'Weekends'
		} else if (cronarray[5].split(',').length > 1) {
			// console.log(', loop 3');
			var daytext = '';
			for (var i = 0; i <= 6; i++) {
				if (cronarray[5].match(i) != null) daytext+=days[i]+',';
			}
			return trim(daytext);
		} else if (cronarray[5].split(',').length == 1) {
			// console.log(', loop 4');
			start = cronarray[5].split(',')[0];
			end = cronarray[5].split(',')[1];
			return start+'-'+end;
		}
		else return null
	};
	var pad = function (n, width, z) {
		z = z || '0';
		n = n + '';
		return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
	};
	var trim = function (stringList) {
		return stringList.replace(/\,$/gi,'');
	};
	var getDaysNames = function (type) {
		switch(type) {
			case 'normal': return ['Sunday','Monday','Tueday','Wednesday','Thursday','Friday','Saturday'];
			case 'micro': return ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
			case 'nano': return ['S','M','T','W','T','F','S'];
			default: return ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
		}
	};
	formatService.toIST = function (time, zone) {
		return moment(time, informat).tz('Asia/Kolkata').format(outformat);
	};
	formatService.toZone = function (time, zone) {
		return moment(time, informat).tz(zone).format(outformat);
	};
	formatService.rawToReadable = function (rawtask, update_flag) {
		// console.log('rawToReadable(); called');
		var readableTask = rawtask;
		console.log('rawtask', rawtask);
		if(typeof update_flag === 'undefined'){
			readableTask.ontime = {
				cron: rawtask.on,
				text: cronToTimeText(rawtask.on),
				secs: cronToTimeSecs(rawtask.on)
			};
			readableTask.offtime = {
				cron: rawtask.off,
				text: cronToTimeText(rawtask.off),
				secs: cronToTimeSecs(rawtask.off)
			};
			readableTask.days = {
				bool: cronToDaysBool(rawtask.on),
				text: cronToDaysText(rawtask.on)
			};
		}
		else if(update_flag == true){
			readableTask.ontime = {
				cron: rawtask.ontime.cron,
				text: cronToTimeText(rawtask.ontime.cron),
				secs: cronToTimeSecs(rawtask.ontime.cron)
			};
			readableTask.offtime = {
				cron: rawtask.offtime.cron,
				text: cronToTimeText(rawtask.offtime.cron),
				secs: cronToTimeSecs(rawtask.offtime.cron)
			};
			readableTask.days = {
				bool: cronToDaysBool(rawtask.ontime.cron),
				text: cronToDaysText(rawtask.ontime.cron)
			};	
		}
		delete readableTask.on;
		delete readableTask.off;
		return readableTask;
	};
	formatService.updateCronTime = function (cronstring, secs) {
		// "* 0 21 * * 0,1,2"
		cronarray = cronstring.split(' ');
		var hours = Math.floor(secs/3600);
		var mins = (secs - hours*3600)/60;
		cronarray[2] = hours;
		cronarray[1] = mins;
		return _.join(cronarray, ' ');
	};
	formatService.updateCronDays = function (cronstring, daysbool) {
		// "* 0 21 * * *"
		cronarray = cronstring.split(' '); 
		var days = '';
		if (_.countBy(daysbool)['true'] == 7) {
			cronarray[5] = '*';
		} else {
			for (i=0; i<=6; i++) {
				if (daysbool[i]==true) {
					days += String(i)+','
				}
			}
			days = trim(days);
			cronarray[5] = days;
		}
		return _.join(cronarray, ' ');
	};
	formatService.makeCronNextDays = function (daysbool, cronstring) {
		// "* 0 21 * * *"
		cronarray = cronstring.split(' ');
		var days = '';
		if (daysbool[6]==true) days +='0,';
		for (i=0; i<6; i++) {
			if (daysbool[i]==true) {
				days += String(i+1)+','
			}
		}
		days = days.replace(/\,$/gi,'');
		cronarray[5] = days;
		return _.join(cronarray, ' ');
	};
	formatService.getTimeText = function (cronstring) {
		return cronToTimeText(cronstring);
	};
	return formatService;
});


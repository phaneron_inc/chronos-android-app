services.service('ScheduleService', function($q, $http, $rootScope, FormatService, BroadcastService) {
	var scheduleService = this;
	scheduleService.tasks = [];
	scheduleService.init = function () {
		var defer = $q.defer();
		$rootScope.socket.get('/tasks/lights', function (tasks, jwres) {
			if (parseInt(jwres/100) == 4) defer.reject('resource not found');
			else if (parseInt(jwres/100) == 5) defer.reject(JSON.toString(jwres.error));
			else {
				$rootScope.socket.post('/subscribe',  { model: 'task' });
				scheduleService.tasks = tasks;
				for (var i = scheduleService.tasks.length - 1; i >= 0; i--) {
					scheduleService.tasks[i] = getReadableTask(scheduleService.tasks[i]);
				};
				BroadcastService('tasks');
				defer.resolve(true);
			}
		});
		return defer.promise;
	};
	scheduleService.update = function (id) {
		var defer = $q.defer();
		$rootScope.socket.get('/task/lights?id='+id, function (task, jwres) {
			updateTask(task.id, task);
			BroadcastService('task', task.id);
		});
		return defer.promise;
	};
	scheduleService.getTasks = function () {
		return scheduleService.tasks;
	};
	scheduleService.isValidTask = function (id) {
		return -1 != _.findIndex(scheduleService.tasks, function(task) { 
			return task.id == id; });
	};
	scheduleService.daysUpdateValidation = function (daysbool) {
		if (_.countBy(daysbool)['true'] == 0) return false;
		else return true;
	};
	scheduleService.getTask = function (id) {
		return _.find(scheduleService.tasks, function (task, jwres) {
			return task.id == id;
		});
	};
	scheduleService.toggle = function (id) {
		$rootScope.socket.put('/task/toggle', {id: id}, function (task, jwres) {
			updateTask(id, { state: state });
		})
	};
	scheduleService.updateOnTime = function (id, time) {
		$rootScope.socket.put('/task', {id: id, on: time}, function (task, jwres) {
			BroadcastService('task.cron', id);
			updateTask(id, { on: task.on });
		});
	};
	scheduleService.updateOffTime = function (id, time) {
		$rootScope.socket.put('/task', {id: id, off: time}, function (task, jwres) {
			BroadcastService('task.cron', id);
			updateTask(id, { off: task.off });
		});
	};
	scheduleService.setCron = function (id, on, off) {
		console.log(id);
		console.log(on);
		console.log(off);
		$rootScope.socket.put('/task', {id: id, on:on, off:off}, function (task, jwres) {
			console.log(id, task);
			BroadcastService('task', id);
			updateTask(id, {on: task.on, off: task.off}, true);
		});
	};
	scheduleService.switchOn = function (id) {
		$rootScope.socket.put('/task/on', {id: id}, function (task, jwres) {
			updateTask(id, { state: task.state });
		});
	};
	scheduleService.switchOff = function (id) {
		$rootScope.socket.put('/task/off', {id: id}, function (task, jwres) {
			updateTask(id, { state: task.state });
		});
	};

	var updateTaskLights = function(id, lights){
		if(typeof id !== 'undefined'){
			scheduleService.lights.map(function (task) {
				if (task.id === id) {
						task.lights = lights;
				}
				console.log('taskLights', task);
				if(typeof update_flag === 'undefined'){
					return FormatService.rawToReadable(task);
				}
				else if(typeof update_flag == true){
					return FormatService.rawToReadable(task, true);
				}
			});
		}
		// else if(typeof id === 'undefined'){
		// 	scheduleService.tasks.map(function(task){
		// 		scheduleService.lights[task.id]
		// 	})
		// }
		console.log('updated', scheduleService.tasks);
		BroadcastService('task', id);
	}

	var updateTask = function (id, options, update_flag) {
		scheduleService.tasks.map(function (task) {
			if (task.id === id) {
				for (prop in options) {
					task[prop] = options[prop];
				}
			}
			console.log('task', task);
			if(typeof update_flag === 'undefined'){
				return FormatService.rawToReadable(task);
			}
			else if(typeof update_flag == true){
				return FormatService.rawToReadable(task, true);
			}
		});
		console.log('updated', scheduleService.tasks);
		BroadcastService('task', id);
	};
	var getReadableTask = function (task) {
		return FormatService.rawToReadable(task);
	};
});
services.service('ConnectionService', function($q, $http, $rootScope, $timeout, LightService, ScheduleService) {
	// initializations
	connectionService = this;
	$rootScope.socket;
	$rootScope.connectionCount;
	var timer;
	// params
	$rootScope.servers = {
		local: { ip: '127.0.0.1', port: '1338' },
		remote: { ip: '127.0.0.1', port: '1338' }
	};
	connectionService.maxtime = 10000;
	connectionService.retries = 30;

	// initializtion
	connectionService.init = function (opts) {
		var defer = $q.defer();
		var maxtime = (opts && opts.maxtime) ? opts.maxtime : connectionService.maxtime;
		var retries = (opts && opts.retries) ? opts.retries : connectionService.retries;
		var callbacks = {
			success: function (res) {defer.resolve(res);},
			failure: function (error) {defer.reject(error);},
			update: function (update) {defer.notify(update);}
		}
		connector(maxtime, retries, 'local')
		.then(
			callbacks.success,
			function () {
				if (!$rootScope.socket.isConnected()) connector(maxtime, retries, 'remote')
				.then(callbacks.success,callbacks.failure,callbacks.update)
			},
			callbacks.update
		)
		return defer.promise;
	};
	// recursive connection logic
	var connector = function (maxtime, retries, servertype) {
		console.log('connector called');
		console.log('retries: ', retries);
		$rootScope.server = 'http://'+$rootScope.servers[servertype].ip+':'+$rootScope.servers[servertype].port;
		var defer = $q.defer();
		$rootScope.connectionCount=0;
		var connect = function () {
			console.log('connection function called!');
			$rootScope.socket = io.sails.connect($rootScope.server,{reconnection: false})
			.on('connect', function() {
				$timeout.cancel(timer);
				defer.resolve({ url:$rootScope.server, retry:($rootScope.connectionCount+1), status:'connected' });
				LightService.init();
				ScheduleService.init();
				// $rootScope.socket.on('light', function (result) {
				// 	console.log(result);
				// 	switch (result.verb) {
				// 		case 'updated': LightService.update(result.data.id); break;
				// 		default: break;
				// 	}
				// });
			}).on('disconnect', function () {
				console.log('disconnect called');
				$rootScope.connectionCount=0;
				connect();
			}).on('error', function (reason) {
				defer.reject({ url:$rootScope.server, retry:(-1), status:'error', reason:reason })
			}).on('light', function (result) {
				console.log(result);
				switch (result.verb) {
					case 'updated': LightService.update(result.id); break;
					default: break;
				}
			}).on('task', function (result) {
				console.log(result);
				switch (result.verb) {
					case 'updated': ScheduleService.update(result.id); break;
					default: break;
				}
			});
			timer = $timeout(function () {
				console.log('timedout in iteration #',$rootScope.connectionCount);
				$rootScope.connectionCount++;
				if (!$rootScope.socket.isConnected()) {
					if ($rootScope.connectionCount<retries) {
						defer.notify({ url:$rootScope.server, retry:($rootScope.connectionCount), status:'failed' });
						connect();
					} else if ($rootScope.connectionCount==retries) {
						defer.reject({ url:$rootScope.server, retry:($rootScope.connectionCount), status:'timedout' });
					}
					else return;
				} else {
					i = 0;
				}
			}, maxtime);
		}
		connect();
		return defer.promise;
	};
});
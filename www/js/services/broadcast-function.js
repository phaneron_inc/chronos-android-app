services.factory('BroadcastService', function($rootScope) {
	return function(msg, data) {
		$rootScope.$broadcast(msg, data);
	}
});